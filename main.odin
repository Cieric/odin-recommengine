package recommengine;

import "core:math"
import "core:fmt"
import "adv_net"

Pair :: struct {
    first : int,
    second : int,
};

PearsonSimilarity :: proc(arr : [][]u32) -> f32 {
    n := f32(len(arr));
    sumX, sumY, sumXY : f32 = 0.0, 0.0, 0.0;
    sqrSumX, sqrSumY : f32 = 0.0, 0.0;
    for elem in arr {
        X := f32(elem[0]);
        Y := f32(elem[1]);
        sumX += X;
        sumY += Y;
        sumXY += X * Y;
        sqrSumX += X * X;
        sqrSumY += Y * Y;
    }
    return max(min((n*sumXY-sumX*sumY) / math.sqrt((n*sqrSumX-sumX*sumX) * (n*sqrSumY-sumY*sumY)), 1), -1);
}

main :: proc()
{
    //sendQuery();

    adv_net.http_request("graphql.anilist.co", nil);
}