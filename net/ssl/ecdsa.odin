package ssl;

import mem "core:mem";
import strings "core:strings";
import fmt "core:fmt";

BigNumber :: struct(N : int, T : typeid)
{
    data : [N/8/size_of(T)]T,
}

assign :: proc(num : ^BigNumber($S, $T), data : [$U]T)
    where S == U * size_of(T) * 8
{
    for val, i in data do num.data[i] = val;
}

add :: proc(lhs, rhs : BigNumber($S, $T)) -> BigNumber(S, T)
{
    size :: S/8/size_of(T);
    last :: size - 1;
    carry : BigNumber(S, T) = {};
    sum : BigNumber(S, T) = lhs;
    for i:=last; i>=0; i-=1 {
        crr : T = carry.data[(i+1)%size];
        sum.data[i] = lhs.data[i] + rhs.data[i] + crr;
        fmt.println(sum.data[i], lhs.data[i], rhs.data[i], crr, (sum.data[i] < lhs.data[i]) ? 1 : 0);
        carry.data[i] = (sum.data[i]-crr < lhs.data[i] || sum.data[i] < lhs.data[i]) ? 1 : 0;
    }
    return sum;
}

invert :: proc(rhs : BigNumber($S, $T)) -> BigNumber(S, T)
{
    out : BigNumber(S, T) = {};
    for val,i in rhs.data {
        out.data[i] = ~val;
    }
    return out;
}

negate :: proc(rhs : BigNumber($S, $T)) -> BigNumber(S, T)
{
    temp : BigNumber(S, T) = {};
    temp.data[S/8/size_of(T)-1] = 0x1;
    return add(invert(rhs), temp);
}

sub :: proc(lhs, rhs : BigNumber($S, $T)) -> BigNumber(S, T)
{
    nrhs = negate(rhs);
    return add(lhs, nrhs);
}

EC :: struct
{
    n : BigNumber(256, u64),
}

make_EC :: proc() -> EC
{
    ec : EC;
    ec.n = {[4]u64{0x1000000000000000, 0x0000000000000000, 0x14DEF9DEA2F79CD6, 0x5812631A5CF5D3ED}};
    return ec;
}

main :: proc()
{
    //ec := make_EC();
    //fmt.println(ec);
    // one : BigNumber(256, u64) = {[4]u64{0x0,0x0,0x0,0x1}};
    // n_one : BigNumber(256, u64) = negate(one);
    // two : BigNumber(256, u64) = {[4]u64{0x0,0x0,0x0,0x2}};
    // n_two : BigNumber(256, u64) = negate(two);

    fmt.println("--------------------------------------------------------------------------------------------------");
    // n_three := add(n_two, n_one);
    // fmt.println(n_three);
    // fmt.println("--------------------------------------------------------------------------------------------------");
    // three := negate(n_three);
    // fmt.println(three);
    // fmt.println("--------------------------------------------------------------------------------------------------");

    // fmt.println(add(n_three, three));
}