package net;

foreign import "system:Ws2_32.lib";
import mem "core:mem";

WORD :: u16;
DWORD :: u16;
BYTE :: u8;
size_t :: u64;

MAKEWORD :: proc(high,low : BYTE) -> WORD {
    return WORD(high << 8) | WORD(low);
}

AF :: enum i32 {
    UNSPEC = 0,
    UNIX = 1,
    INET = 2,
    IMPLINK = 3,
    PUP = 4,
    CHAOS = 5,
    NS = 6,
    IPX = 6,
    ISO = 7,
    OSI = 7,
    ECMA = 8,
    DATAKIT = 9,
    CCITT = 10,
    SNA = 11,
    DECnet = 12,
    DLI = 13,
    LAT = 14,
    HYLINK = 15,
    APPLETALK = 16,
    NETBIOS = 17,
    VOICEVIEW = 18,
    FIREFOX = 19,
    UNKNOWN1 = 20,
    BAN = 21,
    ATM = 22,
    INET6 = 23,
    CLUSTER = 24,
    _12844 = 25,
    IRDA = 26,
    NETDES = 28,
}

SOCK :: enum i32 {
    STREAM = 1,
    DGRAM = 2,
    RAW = 3,
    RDM = 4,
    SEQPACKET = 5,
}

SD_RECEIVE :: 0x00;
SD_SEND :: 0x01;
SD_BOTH :: 0x02;

IPPROTO :: enum i32 {
    HOPOPTS       = 0,
    ICMP          = 1,
    IGMP          = 2,
    GGP           = 3,
    IPV4          = 4,
    ST            = 5,
    TCP           = 6,
    CBT           = 7,
    EGP           = 8,
    IGP           = 9,
    PUP           = 12,
    UDP           = 17,
    IDP           = 22,
    RDP           = 27,
    IPV6          = 41,
    ROUTING       = 43,
    FRAGMENT      = 44,
    ESP           = 50,
    AH            = 51,
    ICMPV6        = 58,
    NONE          = 59,
    DSTOPTS       = 60,
    ND            = 77,
    ICLFXBM       = 78,
    PIM           = 103,
    PGM           = 113,
    L2TP          = 115,
    SCTP          = 132,
    RAW           = 255,
    MAX           = 256,
    RESERVED_RAW  = 257,
    RESERVED_IPSEC  = 258,
    RESERVED_IPSECOFFLOAD  = 259,
    RESERVED_WNV = 260,
    RESERVED_MAX  = 261
};


AI :: enum i32 {
    PASSIVE = 0x01,
    CANONNAME = 0x02,
    NUMERICHOST = 0x04,
    ALL = 0x0100,
    ADDRCONFIG = 0x0400,
    V4MAPPED = 0x0800,
    NON_AUTHORITATIVE = 0x04000,
    SECURE = 0x08000,
    RETURN_PREFERRED_NAMES = 0x010000,
    FQDN = 0x00020000,
    FILESERVER = 0x00040000,
};

WSADESCRIPTION_LEN :: 256;
WSASYS_STATUS_LEN :: 128;

WSAData :: struct {
    wVersion : WORD,
    wHighVersion : WORD,
    iMaxSockets : u16,
    iMaxUdpDg : u16,
    lpVendorInfo : ^byte,
    szDescription : [WSADESCRIPTION_LEN+1]byte,
    szSystemStatus : [WSASYS_STATUS_LEN+1]byte,
}
WSADATA :: WSAData;
LPWSADATA :: ^WSAData;

ADDRESS_FAMILY :: u16;

Sockaddr :: struct {
    sa_family : ADDRESS_FAMILY,
    sa_data : [14]byte,
};

SOCKADDR :: Sockaddr;
LPSOCKADDR :: ^Sockaddr;

Addrinfo :: struct {
    ai_flags : AI,
    ai_family : AF,
    ai_socktype : SOCK,
    ai_protocol : IPPROTO,
    ai_addrlen : size_t,
    ai_canonname : cstring,
    ai_addr : ^Sockaddr,
    ai_next : ^Addrinfo,
};
ADDRINFOA :: Addrinfo;
PADDRINFOA :: ^Addrinfo;

SOCKET :: uintptr;

INVALID_SOCKET :: ~SOCKET(0);
SOCKET_ERROR :: -1;

SOMAXCONN :: 128;

PCSTR :: cstring;

@(default_calling_convention="std")
foreign Ws2_32 {
    @(link_name="WSAStartup") wsa_startup :: proc (wVersionRequested : WORD, lpWSAData : LPWSADATA) -> i32 ---;
    @(link_name="WSACleanup") wsa_cleanup :: proc () -> i32 ---;
    @(link_name="WSAGetLastError") wsa_get_last_error :: proc () -> i32 ---;
    @(link_name="getaddrinfo") get_addrinfo :: proc (pNodeName, pServiceName : PCSTR, pHints : ^ADDRINFOA, ppResult : ^PADDRINFOA) -> i32 ---;
    @(link_name="freeaddrinfo") free_addrinfo :: proc (pAddrinfo : PADDRINFOA) ---;
    @(link_name="socket") socket :: proc (af : AF, type : SOCK, protocol : IPPROTO) -> SOCKET ---;
    @(link_name="connect") connect :: proc (s : SOCKET, name : ^Sockaddr, namelen : i32 ) -> i32 ---;
    @(link_name="closesocket") close_socket :: proc (s : SOCKET) -> i32 ---;
    @(link_name="send") c_send :: proc (s : SOCKET, buf : cstring, len : i32, flags : i32) -> i32 ---;
    @(link_name="recv") recv :: proc (s : SOCKET, buf : ^byte, len : i32, flags : i32) -> i32 ---;
    @(link_name="shutdown") shutdown :: proc (s : SOCKET, how : int) -> i32 ---;

    @(link_name="listen") listen :: proc (s : SOCKET, backlog : i32) -> i32 ---;
    @(link_name="accept") accept :: proc (s : SOCKET, addr : ^Sockaddr, addrlen : ^i32) -> SOCKET ---;
    @(link_name="bind") bind :: proc(s : SOCKET, name : ^Sockaddr, namelen : i32) -> i32 ---;
}

o_send :: proc (s : SOCKET, buf : string, flags : i32) -> i32
{
    return c_send(s, cast(cstring)(mem.raw_data(buf)), cast(i32)(len(buf)), flags);
};

send :: proc{c_send, o_send};