package net;

import "core:mem"
import "core:fmt"

wsa_init :: proc()
{
    data: WSAData;
    err := wsa_startup(MAKEWORD(2, 2), &data);
    assert(err == 0);
}

wsa_clean :: proc()
{
    wsa_cleanup();
}

tcp_data :: struct {
    socket : SOCKET,
}

tcp_connect :: proc(pipe : ^Pipe, host : cstring, port : cstring) -> i32
{
    addrinfo: ^Addrinfo;
    hints: Addrinfo;
    hints.ai_family   = .INET;
    hints.ai_socktype = .STREAM;
    hints.ai_protocol = .TCP;

    if err := get_addrinfo(host, port, &hints, &addrinfo); err != 0  do return err;
    defer free_addrinfo(addrinfo);

    socket := socket(addrinfo.ai_family, addrinfo.ai_socktype, addrinfo.ai_protocol);
    if socket == INVALID_SOCKET  do return wsa_get_last_error();
    assert(socket != 0);
    defer close_socket(socket);

    connect_result := connect(socket, addrinfo.ai_addr, cast(i32)addrinfo.ai_addrlen);
    if connect_result == SOCKET_ERROR do return wsa_get_last_error();

    data := cast(^tcp_data)pipe.data;
    data.socket = socket;

    return 0;
}

tcp_shutdown :: proc(pipe : ^Pipe) -> i32
{
    data := cast(^tcp_data)pipe.data;
    shutdown_result := shutdown(data.socket, SD_SEND);
    if shutdown_result == SOCKET_ERROR do return wsa_get_last_error();
    return 0;
}

tcp_send :: proc(pipe : ^Pipe, buffer : []u8) -> i32
{
    data := cast(^tcp_data)pipe.data;
    return send(data.socket, cast(string)buffer, 0);
}

tcp_recv :: proc(pipe : ^Pipe, buffer : []u8) -> i32
{
    data := cast(^tcp_data)pipe.data;
    out := recv(data.socket, cast(^u8)&buffer[0], i32(len(buffer)), 0);
    if out == -1 do return wsa_get_last_error();
    return 0;
}

default_tcp_pipe :: proc() -> Pipe
{
    output : Pipe;
    wsa_init();
    output.data = cast(^u8)mem.alloc(size_of(tcp_data));
    output.connect = tcp_connect;
    output.shutdown = tcp_shutdown;
    output.send = tcp_send;
    output.recv = tcp_recv;
    return output;
}