package adv_net;

import "core:strings"
import "core:fmt"
import net "../net"

http_options :: struct {
    method : string,
    headers : map[string]string,
    body : string,
    pipe : ^net.Pipe
};

get_path :: proc(url : string) -> cstring
{
    return "/";
}

get_host :: proc(url : string) -> cstring
{
    return "graphql.anilist.co";
}

get_protocol :: proc(url : string) -> cstring
{
    return "http";
}

get_port :: proc(url : string) -> cstring
{
    return "80";
}

http_request :: proc(url : string, options : ^http_options)
{
    pipe : net.Pipe;
    if options != nil && options.pipe != nil do pipe = options.pipe^;
    else do pipe = net.default_tcp_pipe();

    port := get_port(url);
    host := get_host(url);
    path := get_path(url);

    builder := strings.make_builder();
    defer strings.destroy_builder(&builder);

    strings.write_string(&builder, "GET ");
    strings.write_string(&builder, string(path));
    strings.write_string(&builder, " HTTP/1.1\r\n"); // Write Version
    strings.write_string(&builder, "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n");
    strings.write_string(&builder, "Host: ");
    strings.write_string(&builder, string(host));
    strings.write_string(&builder, "\r\n");
    strings.write_string(&builder, "Accept-Language: en-us\r\n");
    strings.write_string(&builder, "Accept-Encoding: application/json\r\n");
    strings.write_string(&builder, "Content-Type: application/json\r\n");
    strings.write_string(&builder, "Connection: Keep-Alive\r\n");
    strings.write_string(&builder, "\r\n"); //END HEADER

    pipe.connect(&pipe, host, port);


    header := strings.to_string(builder);
    fmt.println(header);
    pipe.send(&pipe, transmute([]u8)(header));

    //
    @static recv_buffer: [16384]byte;
    receive_loop:
    for {
        receive_result := pipe.recv(&pipe, recv_buffer[:]);
        switch {
            case receive_result > 0: {
                fmt.println("Received from server: ", cast(string)recv_buffer[:receive_result]);
                break receive_loop;
            }
            case receive_result == 0: {
                break receive_loop;
            }
            case: {
                fmt.println("Receive failed: ", receive_result);
                return;
            }
        }
    }

    //
    pipe.shutdown(&pipe);

    //
    fmt.println("Connection closed");
}