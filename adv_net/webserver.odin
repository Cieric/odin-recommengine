package adv_net

/* Courtesy of shwa from odin discord. */

import "core:fmt"
import "core:strings"
import "core:os"
import "core:sys/win32"
import net "../net"

DEFAULT_PORT :: "15234";

main :: proc() {
    data: net.WSAData;
    err := net.wsa_startup(net.MAKEWORD(2, 2), &data);
    assert(err == 0);
    defer net.wsa_cleanup();

    if array_contains(os.args, "server") {
        fmt.println("Server listening on", DEFAULT_PORT);

        //
        result, ptr: ^net.Addrinfo;
        hints: net.Addrinfo;
        hints.ai_family   = .INET;
        hints.ai_socktype = .STREAM;
        hints.ai_protocol = .TCP;
        hints.ai_flags    = .PASSIVE;

        // Resolve the local address and port to be used by the server
        err2 := net.get_addrinfo(nil, DEFAULT_PORT, &hints, &result);
        defer net.free_addrinfo(result); // todo(josh): we can actually call this after binding
        if (err2 != 0) {
            fmt.println("getaddrinfo failed: ", err2);
            return;
        }

        //
        socket := net.socket(result.ai_family, result.ai_socktype, result.ai_protocol);
        if socket == net.INVALID_SOCKET {
            fmt.println("socketproc failed: ", win32.get_last_error());
            return;
        }
        assert(socket != 0);
        defer net.close_socket(socket);

        //
        bindres := net.bind(socket, result.ai_addr, cast(i32)result.ai_addrlen);
        if bindres == net.SOCKET_ERROR {
            fmt.println("bindproc failed: ", net.wsa_get_last_error());
            return;
        }

        //
        listen_err := net.listen(socket, net.SOMAXCONN);
        if listen_err == net.SOCKET_ERROR {
            fmt.println("listen failed: ", net.wsa_get_last_error());
            return;
        }

        for {
            fmt.println("Listening...");
            client := net.accept(socket, nil, nil);
            fmt.println("Accepted client.");
            if client == net.INVALID_SOCKET {
                fmt.println("accept failed: ", net.wsa_get_last_error());
                continue;
            }
            defer net.close_socket(client);

            send_string(client, "HTTP/1.0 200 OK\r\n\x00");
            send_string(client, "Server: shwa server/0.1.0\r\n\x00");
            send_string(client, "Content-Type: text/html\r\n\x00");
            send_string(client, "\r\n\x00");
            send_string(client, "<HTML><TITLE>WOW IT WORKS</TITLE></HTML>\r\n\x00");
            send_string(client, "<BODY>ODIN HTTP SERVER BABYYYY</BODY>\r\n\x00");

            shutdown_result := net.shutdown(client, net.SD_SEND);
            if shutdown_result == net.SOCKET_ERROR {
                fmt.println("shutdown failed: ", win32.get_last_error());
                continue;
            }
        }
    }
    else {
        fmt.println("Client!");

        //
        result, ptr: ^net.Addrinfo;
        hints: net.Addrinfo;
        hints.ai_family   = .INET;
        hints.ai_socktype = .STREAM;
        hints.ai_protocol = .TCP;
        // hints.ai_flags    = AI_PASSIVE;

        // Resolve the local address and port to be used by the server
        err2 := net.get_addrinfo("localhost", DEFAULT_PORT, &hints, &result);
        defer net.free_addrinfo(result); // todo(josh): we can actually call this after binding
        if err2 != 0 {
            fmt.println("getaddrinfo failed: ", err2);
            return;
        }
        ptr = result;

        //
        socket := net.socket(ptr.ai_family, ptr.ai_socktype, ptr.ai_protocol);
        if socket == net.INVALID_SOCKET {
            fmt.println("socketproc failed: ", win32.get_last_error());
            return;
        }
        assert(socket != 0);
        defer net.close_socket(socket);

        //
        connect_result := net.connect(socket, ptr.ai_addr, cast(i32)ptr.ai_addrlen);
        if connect_result == net.SOCKET_ERROR {
            fmt.println("connect() failed: ", win32.get_last_error());
            return;
        }
        fmt.println("Connected to server.");

        //
        // send_string(socket, "Hello, Server!");

        //
        @static recv_buffer: [1024]byte;
        receive_loop2:
        for {
            receive_result := net.recv(socket, cast(^u8)&recv_buffer[0], len(recv_buffer), 0);
            switch {
                case receive_result > 0: {
                    fmt.println("Received from server: ", cast(string)recv_buffer[:receive_result]);
                    break receive_loop2;
                }
                case receive_result == 0: {
                    break receive_loop2;
                }
                case: {
                    fmt.println("Receive failed: ", net.wsa_get_last_error());
                    return;
                }
            }
        }

        //
        shutdown_result := net.shutdown(socket, net.SD_SEND);
        if shutdown_result == net.SOCKET_ERROR {
            fmt.println("shutdown failed: ", net.wsa_get_last_error());
            return;
        }

        //
        fmt.println("Connection closed");
    }
}

send_string :: proc(client: net.SOCKET, str: string) {
    buf: [1024]byte;
    bprintf(buf[:], str);
    send_result := net.send(client, cast(cstring)&buf[0], cast(i32)len(cast(cstring)&buf[0]), 0);
    if send_result == net.SOCKET_ERROR {
        fmt.println("Send error: ", win32.get_last_error());
    }
}

array_contains :: proc(array: []$E, val: E) -> bool {
    for elem in array {
        if elem == val {
            return true;
        }
    }
    return false;
}

println :: fmt.println;
printf :: fmt.printf;
bprintf :: fmt.bprintf;
sbprintf :: fmt.sbprintf;